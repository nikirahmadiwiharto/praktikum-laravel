<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Student;

class StudentController extends Controller
{
    public function index() {
        $student = Student::all();
        return view('student')->with("murid", $student);
        // return view('student')->with([
        //     "student" => $student,
        //     ""
        // ]);
    }
}
