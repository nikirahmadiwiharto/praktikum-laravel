<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Book;

class HomeController extends Controller
{
    public function form() {
        return view("form");
    }

    public function proses(Request $input) {
        $book = new Book;
        $book->judul = $input->judul;
        $book->pengarang = $input->pengarang;
        $book->save();
        return back();
    }

    public function listBook()
    {
        $book = Book::all();
        return view("book.list")
                ->with("book", $book);
    }

    public function formEdit($id)
    {
        $book = Book::find($id);
        return view("book.edit")
                ->with("book", $book);
    }

    public function formEditPost(Request $input, $id)
    {
        $book = Book::find($id);
        $book->judul = $input->judul;
        $book->pengarang = $input->pengarang;
        $book->save();
        return redirect()->route("listbook");
    }




}
