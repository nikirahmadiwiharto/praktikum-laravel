<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>
            @yield('title')
        </title>
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    </head>
    <body>
        <div>
            MENU ATAS
        </div>

        @yield('content')

        <div>
            FOOTER
        </div>
    </body>
</html>
