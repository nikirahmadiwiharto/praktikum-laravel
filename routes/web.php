<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
// Route
//     - Endpoin
//     - Callback
//     - Name
//     - With Controller
//     - With out Controller
// Controller
//     - Namespace
//     - Controller name & file name
//     - view function
// View
//     - Blade
//     - include
//
// - Stefanus
// - Santi
// - Harsa
// - Anastasia
// - Dara
// - Tantut
// - Fahrian
// - Gabil
// - Roni
// - Amel
// - Ayi
// - Hafiza
// - Megi
// - Tetra

Route::get('/', 'HomeController@index')->name('index');

Route::get('/homess1', function () {
    return view('welcome');
})->name('beranda');

Route::get('/about', 'HomeController@about')->name('about');
Route::get('/contact', 'Dir\HomeController@contact');

Route::view('/welcome', 'welcome');

Route::get("/bootstrap/home", "BootstrapController@index")
        ->name('bootstrap.index');

Route::get("/student", "StudentController@index");

Route::get("/form", "HomeController@form");
Route::post("/form", "HomeController@proses")
        ->name("proses");

Route::get("/book", "HomeController@listBook")
        ->name("listbook");
Route::get("formEdit/{id}", "HomeController@formEdit")
        ->name("formedit");
Route::post("formEdit/{id}", "HomeController@formEditPost")
        ->name("formeditpost");







//
